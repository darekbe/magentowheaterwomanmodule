<?php

namespace Appli5\Weatherwoman\Model\ResourceModel\WeatherArchive;

use Appli5\Weatherwoman\Model as WeatherwomanModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct(): void
    {
        $this->_init(WeatherwomanModel\WeatherArchive::class, WeatherwomanModel\ResourceModel\WeatherArchive::class);
    }
}
