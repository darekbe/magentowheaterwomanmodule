<?php

namespace Appli5\Weatherwoman\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class WeatherArchive extends AbstractDb
{
    const TABLE_NAME = 'appli5_weatherwoman__archive';

    protected function _construct(): void
    {
        $this->_init(self::TABLE_NAME, 'archive_id');
    }
}
