<?php

namespace Appli5\Weatherwoman\Model;

use Appli5\Weatherwoman\Api\Data\WeatherArchiveInterface;
use Appli5\Weatherwoman\Model as WeatherwomanModel;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class WeatherArchive extends AbstractModel implements WeatherArchiveInterface, IdentityInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'appli5_weatherwoman_archive';

    protected function _construct(): void
    {
        $this->_init(WeatherwomanModel\ResourceModel\WeatherArchive::class);
    }

    public function getContent(): ?string
    {
        return $this->getData(self::CONTENT);
    }

    public function getCreatedAt(): ?string
    {
        return $this->getData(self::CREATED_AT);
    }

    public function getId(): ?int
    {
        return $this->getData(self::ARCHIVE_ID);
    }

    /**
     * @return string[]
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function setContent(string $content): self
    {
        return $this->setData(self::CONTENT, $content);
    }

    public function setCreatedAt(string $createdAt): self
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @param int $id
     */
    public function setId($id): self
    {
        return $this->setData(self::ARCHIVE_ID, $id);
    }
}
