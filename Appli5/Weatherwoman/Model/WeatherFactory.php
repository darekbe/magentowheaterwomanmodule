<?php

namespace Appli5\Weatherwoman\Model;

use Appli5\Weatherwoman\Model\Config\GeneralConfig;
use Appli5\Weatherwoman\Model\Config\Source\Measurementsystem;
use Assert\Assert;

class WeatherFactory
{
    public static function create(WeatherArchive $weatherArchive, GeneralConfig $generalConfig): Weather
    {
        self::checkAssertions($weatherArchive, $generalConfig);

        return new Weather(
            json_decode($weatherArchive->getContent()),
            $generalConfig->getMeasurementSystem()
        );
    }

    private static function checkAssertions(WeatherArchive $weatherArchive, GeneralConfig $generalConfig): void
    {
        Assert::that($weatherArchive->getContent())
            ->isJsonString();

        Assert::that($generalConfig->getMeasurementSystem())
            ->choice(array_keys(Measurementsystem::ENUM));
    }
}
