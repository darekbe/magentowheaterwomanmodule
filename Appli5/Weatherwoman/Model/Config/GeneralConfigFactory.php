<?php

namespace Appli5\Weatherwoman\Model\Config;

use Appli5\Weatherwoman\Api\Data\GeneralConfigInterface;
use Assert\Assert;

class GeneralConfigFactory
{
    public static function createFromArray(array $configData): GeneralConfig
    {
        self::checkAssertions($configData);

        return new GeneralConfig(
            $configData[GeneralConfigInterface::OPTION_ENABLE],
            $configData[GeneralConfigInterface::OPTION_LANGUAGE],
            $configData[GeneralConfigInterface::OPTION_MEASUREMENT_SYSTEM],
            $configData[GeneralConfigInterface::OPTION_API_KEY],
            $configData[GeneralConfigInterface::OPTION_QUERY]
        );
    }

    private static function checkAssertions(array $configData): void
    {
        Assert::that($configData)
            ->keyIsset(GeneralConfigInterface::OPTION_ENABLE)
            ->keyIsset(GeneralConfigInterface::OPTION_LANGUAGE)
            ->keyIsset(GeneralConfigInterface::OPTION_MEASUREMENT_SYSTEM)
            ->keyIsset(GeneralConfigInterface::OPTION_API_KEY)
            ->keyIsset(GeneralConfigInterface::OPTION_QUERY);

        Assert::that($configData[GeneralConfigInterface::OPTION_API_KEY])
            ->notBlank();

        Assert::that($configData[GeneralConfigInterface::OPTION_QUERY])
            ->notBlank();
    }
}
