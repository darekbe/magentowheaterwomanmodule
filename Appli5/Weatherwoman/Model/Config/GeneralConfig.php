<?php

namespace Appli5\Weatherwoman\Model\Config;

use Appli5\Weatherwoman\Api\Data\GeneralConfigInterface;

class GeneralConfig implements GeneralConfigInterface
{
    private bool $enable;
    private string $language;
    private string $measurementSystem;
    private string $apiKey;
    private string $query;

    public function __construct(
        bool   $enable,
        string $language,
        string $measurementSystem,
        string $apiKey,
        string $query
    )
    {
        $this->enable = $enable;
        $this->language = $language;
        $this->measurementSystem = $measurementSystem;
        $this->apiKey = $apiKey;
        $this->query = $query;
    }

    public function isEnable(): bool
    {
        return $this->enable;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function getMeasurementSystem(): string
    {
        return $this->measurementSystem;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }
}
