<?php

namespace Appli5\Weatherwoman\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Language implements OptionSourceInterface
{
    public function toOptionArray(): array
    {
        return [
            ['value' => 'af', 'label' => __('Afrikaans')],
            ['value' => 'al', 'label' => __('Albanian')],
            ['value' => 'ar', 'label' => __('Arabic')],
            ['value' => 'az', 'label' => __('Azerbaijani')],
            ['value' => 'bg', 'label' => __('Bulgarian')],
            ['value' => 'ca', 'label' => __('Catalan')],
            ['value' => 'cz', 'label' => __('Czech')],
            ['value' => 'da', 'label' => __('Danish')],
            ['value' => 'de', 'label' => __('German')],
            ['value' => 'el', 'label' => __('Greek')],
            ['value' => 'en', 'label' => __('English')],
            ['value' => 'eu', 'label' => __('Basque')],
            ['value' => 'fa', 'label' => __('Persian (Farsi)')],
            ['value' => 'fi', 'label' => __('Finnish')],
            ['value' => 'fr', 'label' => __('French')],
            ['value' => 'gl', 'label' => __('Galician')],
            ['value' => 'he', 'label' => __('Hebrew')],
            ['value' => 'hi', 'label' => __('Hindi')],
            ['value' => 'hr', 'label' => __('Croatian')],
            ['value' => 'hu', 'label' => __('Hungarian')],
            ['value' => 'id', 'label' => __('Indonesian')],
            ['value' => 'it', 'label' => __('Italian')],
            ['value' => 'ja', 'label' => __('Japanese')],
            ['value' => 'kr', 'label' => __('Korean')],
            ['value' => 'la', 'label' => __('Latvian')],
            ['value' => 'lt', 'label' => __('Lithuanian')],
            ['value' => 'mk', 'label' => __('Macedonian')],
            ['value' => 'no', 'label' => __('Norwegian')],
            ['value' => 'nl', 'label' => __('Dutch')],
            ['value' => 'pl', 'label' => __('Polish')],
            ['value' => 'pt', 'label' => __('Portuguese')],
            ['value' => 'pt_br', 'label' => __('Português Brasil')],
            ['value' => 'ro', 'label' => __('Romanian')],
            ['value' => 'ru', 'label' => __('Russian')],
            ['value' => 'sv', 'label' => __('Swedish')],
            ['value' => 'sk', 'label' => __('Slovak')],
            ['value' => 'sl', 'label' => __('Slovenian')],
            ['value' => 'sp', 'label' => __('Spanish')],
            ['value' => 'sr', 'label' => __('Serbian')],
            ['value' => 'th', 'label' => __('Thai')],
            ['value' => 'tr', 'label' => __('Turkish')],
            ['value' => 'ua', 'label' => __('Ukrainian')],
            ['value' => 'vi', 'label' => __('Vietnamese')],
            ['value' => 'zh_cn', 'label' => __('Chinese Simplified')],
            ['value' => 'zh_tw', 'label' => __('Chinese Traditional')],
            ['value' => 'zu', 'label' => __('Zulu')],
        ];
    }
}
