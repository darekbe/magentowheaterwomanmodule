<?php

namespace Appli5\Weatherwoman\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;


class Measurementsystem implements OptionSourceInterface
{
    const ENUM = [
        'metric' => 'Metric',
        'imperial' => 'Imperial',
    ];

    public function toOptionArray(): array
    {
        return array_map(function ($value, $label) {
            return [
                'value' => $value,
                'label'  => __($label),
            ];
        }, array_keys(self::ENUM), self::ENUM);
    }
}
