<?php

namespace Appli5\Weatherwoman\Api\Data;

interface WeatherArchiveInterface
{
    const ARCHIVE_ID = 'archive_id';
    const CONTENT = 'content';
    const CREATED_AT = 'created_at';

    public function getContent(): ?string;

    public function getCreatedAt(): ?string;

    public function getId(): ?int;

    public function setContent(string $content): self;

    public function setCreatedAt(string $createdAt): self;

    /**
     * @param int $id
     */
    public function setId($id): self;
}
