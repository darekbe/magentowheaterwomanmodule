<?php

namespace Appli5\Weatherwoman\Api\Data;

interface GeneralConfigInterface
{
    const OPTION_ENABLE = 'enable';
    const OPTION_LANGUAGE = 'language';
    const OPTION_MEASUREMENT_SYSTEM = 'units';
    const OPTION_API_KEY = 'api_key';
    const OPTION_QUERY = 'query';

    public function isEnable(): bool;

    public function getQuery(): string;

    public function getApiKey(): string;

    public function getMeasurementSystem(): string;

    public function getLanguage(): string;
}
