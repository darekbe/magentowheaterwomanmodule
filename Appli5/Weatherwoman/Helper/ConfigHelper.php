<?php

namespace Appli5\Weatherwoman\Helper;

use Appli5\Weatherwoman\Api\Data\GeneralConfigInterface;
use Appli5\Weatherwoman\Model\Config\GeneralConfigFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class ConfigHelper extends AbstractHelper
{
    const CONFIG_PATH = 'weatherwoman/';


    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getGeneralConfig($storeId = null): GeneralConfigInterface
    {
        $generalConfigData = $this->getConfigValue(self::CONFIG_PATH . 'general', $storeId);

        return GeneralConfigFactory::createFromArray($generalConfigData);
    }
}
