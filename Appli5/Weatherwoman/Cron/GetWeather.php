<?php

namespace Appli5\Weatherwoman\Cron;

use Appli5\Weatherwoman\Api\Data\GeneralConfigInterface;
use Appli5\Weatherwoman\Helper\ConfigHelper;
use Appli5\Weatherwoman\Model\ResourceModel\WeatherArchive as WeatherArchiveResource;
use Appli5\Weatherwoman\Model\WeatherArchive as WeatherArchiveModel;
use Cmfcmf\OpenWeatherMap;
use Magento\Framework\ObjectManagerInterface;
use Psr\Http\Client\ClientInterface as HttpClientInterface;
use Psr\Http\Message\RequestFactoryInterface as HttpRequestFactoryInterface;
use Psr\Log\LoggerInterface;


class GetWeather
{
    private ObjectManagerInterface $objectManager;
    private WeatherArchiveResource $weatherArchiveResource;
    private HttpRequestFactoryInterface $httpRequestFactory;
    private HttpClientInterface $httpClient;
    private ConfigHelper $weatherwomanConfigHelper;
    private LoggerInterface $logger;

    public function __construct(
        ObjectManagerInterface      $objectManager,
        WeatherArchiveResource      $weatherArchiveResource,
        HttpRequestFactoryInterface $httpRequestFactory,
        HttpClientInterface         $httpClient,
        ConfigHelper                $weatherwomanConfigHelper,
        LoggerInterface             $logger
    )
    {
        $this->objectManager = $objectManager;
        $this->weatherArchiveResource = $weatherArchiveResource;
        $this->httpRequestFactory = $httpRequestFactory;
        $this->httpClient = $httpClient;
        $this->weatherwomanConfigHelper = $weatherwomanConfigHelper;
        $this->logger = $logger;
    }

    public function execute(): void
    {
        try {
            $weatherData = self::getWeatherDataFromOpenWeatherMap(
                $this->httpRequestFactory,
                $this->httpClient,
                $this->weatherwomanConfigHelper->getGeneralConfig()
            );

            self::saveWeatherArchive($this->objectManager, $this->weatherArchiveResource, $weatherData);

        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());

            throw $exception;
        }
    }

    private static function getWeatherDataFromOpenWeatherMap(
        HttpRequestFactoryInterface $httpRequestFactory,
        HttpClientInterface         $httpClient,
        GeneralConfigInterface      $generalConfig
    ): array
    {
        $owm = new OpenWeatherMap($generalConfig->getApiKey(), $httpClient, $httpRequestFactory);

        $responseResult = $owm->getRawWeatherData(
            $generalConfig->getQuery(),
            $generalConfig->getMeasurementSystem(),
            $generalConfig->getLanguage(),
            null,
            'json'
        );

        if ((bool)$responseResult === false || $responseResult === 'false') {
            throw new \Exception('Error during downloading weather data!');
        }

        return [
            'content' => $responseResult,
            'createdAt' => (new \DateTimeImmutable())->format('Y-m-d H:i:s'),
        ];
    }

    private static function saveWeatherArchive(
        ObjectManagerInterface $objectManager,
        WeatherArchiveResource $weatherArchiveResource,
        array                  $data
    ): void
    {
        /** @var WeatherArchiveModel $weatherArchiveModel */
        $weatherArchiveModel = $objectManager->create(WeatherArchiveModel::class);

        $weatherArchiveModel
            ->setContent($data['content'])
            ->setCreatedAt($data['createdAt']);

        $weatherArchiveResource->save($weatherArchiveModel);
    }
}
