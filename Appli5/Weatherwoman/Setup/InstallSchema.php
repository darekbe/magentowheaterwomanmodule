<?php

namespace Appli5\Weatherwoman\Setup;

use Appli5\Weatherwoman\Api\Data\WeatherArchiveInterface;
use Appli5\Weatherwoman\Model\ResourceModel\WeatherArchive;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {
        $setup->startSetup();

        self::addWeatherwomanArchiveTable($setup);

        $setup->endSetup();
    }

    private static function addWeatherwomanArchiveTable(SchemaSetupInterface $setup): void
    {
        $tableName = $setup->getTable(WeatherArchive::TABLE_NAME);

        if ($setup->getConnection()->isTableExists($tableName) != true) {
            $table = $setup->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    WeatherArchiveInterface::ARCHIVE_ID,
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    WeatherArchiveInterface::CONTENT,
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Content'
                )
                ->addColumn(
                    WeatherArchiveInterface::CREATED_AT,
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->setComment('Appli5 Weatherwoman - Archive');
            $setup->getConnection()->createTable($table);
        }
    }
}
