<?php

namespace Appli5\Weatherwoman\Setup;

use Appli5\Weatherwoman\Model\ResourceModel\WeatherArchive;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;


class UpgradeData implements UpgradeDataInterface
{
    private static function insertExampleWeatherData(ModuleDataSetupInterface $setup): void
    {
        $tableName = $setup->getTable(WeatherArchive::TABLE_NAME);

        $setup
            ->getConnection()
            ->insertMultiple($tableName, self::getExampleWeatherDate());
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context): void
    {
        $setup->startSetup();

        self::insertExampleWeatherData($setup);

        $setup->endSetup();
    }

    public static function getExampleWeatherDate(): array
    {
        $currentDateString = (new \DateTimeImmutable())->format('Y-m-d');

        return [
            [
                'content' => '{"coord":{"lon":23,"lat":51},"weather":[{"id":803,"main":"Clouds","description":"zachmurzenie umiarkowane","icon":"04d"}],"base":"stations","main":{"temp":-5.24,"feels_like":-11.17,"temp_min":-5.99,"temp_max":-5.23,"pressure":1015,"humidity":80,"sea_level":1015,"grnd_level":985},"visibility":10000,"wind":{"speed":4.58,"deg":80,"gust":7.2},"clouds":{"all":80},"dt":1638966245,"sys":{"type":1,"id":1702,"country":"PL","sunrise":1638944283,"sunset":1638973325},"timezone":3600,"id":858785,"name":"Województwo lubelskie","cod":200}',
                'created_at' => $currentDateString . ' 12:26:40',
            ],
            [
                'content' => '{"coord":{"lon":23,"lat":51},"weather":[{"id":801,"main":"Clouds","description":"pochmurnie","icon":"02d"}],"base":"stations","main":{"temp":-5.24,"feels_like":-10.55,"temp_min":-5.99,"temp_max":-5.23,"pressure":1014,"humidity":85,"sea_level":1014,"grnd_level":985},"visibility":10000,"wind":{"speed":3.8,"deg":75,"gust":7.1},"clouds":{"all":22},"dt":1638967280,"sys":{"type":1,"id":1702,"country":"PL","sunrise":1638944283,"sunset":1638973325},"timezone":3600,"id":858785,"name":"Województwo lubelskie","cod":200}',
                'created_at' => $currentDateString . ' 12:44:06',
            ],
            [
                'content' => '{"coord":{"lon":23,"lat":51},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"base":"stations","main":{"temp":-5.28,"feels_like":-9.81,"temp_min":-7.1,"temp_max":-5.23,"pressure":1014,"humidity":96,"sea_level":1014,"grnd_level":985},"visibility":811,"wind":{"speed":2.97,"deg":98,"gust":8.23},"clouds":{"all":69},"dt":1638986947,"sys":{"type":1,"id":1702,"country":"PL","sunrise":1638944283,"sunset":1638973325},"timezone":3600,"id":858785,"name":"Lublin Voivodeship","cod":200}',
                'created_at' => $currentDateString . ' 18:11:25',
            ],
            [
                'content' => '{"coord":{"lon":23,"lat":51},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"base":"stations","main":{"temp":-6.25,"feels_like":-11.81,"temp_min":-7.1,"temp_max":-6.23,"pressure":1014,"humidity":95,"sea_level":1014,"grnd_level":984},"visibility":10000,"wind":{"speed":3.82,"deg":93,"gust":7.9},"clouds":{"all":87},"dt":1638996182,"sys":{"type":1,"id":1702,"country":"PL","sunrise":1638944283,"sunset":1638973325},"timezone":3600,"id":858785,"name":"Lublin Voivodeship","cod":200}',
                'created_at' => $currentDateString . ' 20:44:05',
            ],
            [
                'content' => '{"coord":{"lon":22.2271,"lat":50.9236},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"base":"stations","main":{"temp":-5.39,"feels_like":-11,"temp_min":-6.92,"temp_max":-3.18,"pressure":1013,"humidity":94,"sea_level":1013,"grnd_level":987},"visibility":10000,"wind":{"speed":4.12,"deg":104,"gust":9.31},"clouds":{"all":68},"dt":1638996908,"sys":{"type":2,"id":2000448,"country":"PL","sunrise":1638944447,"sunset":1638973532},"timezone":3600,"id":767623,"name":"Kraśnik","cod":200}',
                'created_at' => $currentDateString . ' 21:00:04',
            ],
        ];
    }
}
