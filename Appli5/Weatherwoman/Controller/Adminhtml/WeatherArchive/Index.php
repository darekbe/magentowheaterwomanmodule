<?php

namespace Appli5\Weatherwoman\Controller\Adminhtml\WeatherArchive;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $resultPageFactory;

    public function __construct(
        Action\Context $context,
        PageFactory    $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;

        return parent::__construct($context);
    }

    public function execute(): Page
    {
        $page = $this->resultPageFactory->create();
        $page->getConfig()->getTitle()->prepend('Weatherwoman Weather Archive');

        return $page;
    }

    protected function _isAllowed(): bool
    {
        return $this->_authorization->isAllowed('Appli5_Weatherwoman::weatherwoman_weather_archives');
    }
}
