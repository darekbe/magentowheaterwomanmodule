<?php

namespace Appli5\Weatherwoman\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;

class LinkToFrontendCurrentWeatherPage extends Field
{
    public function __construct(
        Context $context,
        array   $data = []
    )
    {
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element): string
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    protected function _getElementHtml(AbstractElement $element): string
    {
        return sprintf(
            '<a href ="%s" target="_blank">%s</a>',
            $this->_urlBuilder->getBaseUrl() . 'weatherwoman/index/index',
            __('Frontend Current Weather Page')
        );
    }
}
