<?php

namespace Appli5\Weatherwoman\Block;

use Appli5\Weatherwoman\Api\Data\WeatherArchiveInterface;
use Appli5\Weatherwoman\Helper\ConfigHelper;
use Appli5\Weatherwoman\Model\ResourceModel\WeatherArchive\Collection as WeatherArchiveCollection;
use Appli5\Weatherwoman\Model\ResourceModel\WeatherArchive\CollectionFactory as WeatherArchiveCollectionFactory;
use Appli5\Weatherwoman\Model\WeatherArchive as WeatherArchiveModel;
use Appli5\Weatherwoman\Model\WeatherFactory;
use Cmfcmf\OpenWeatherMap\CurrentWeather;
use Magento\Framework\Api\CriteriaInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class WeatherArchive extends Template
{
    /**
     * @var null|WeatherArchiveCollectionFactory
     */
    protected $weatherArchiveCollectionFactory = null;
    private ConfigHelper $configHelper;

    public function __construct(
        Context                         $context,
        WeatherArchiveCollectionFactory $weatherArchiveCollectionFactory,
        ConfigHelper                    $configHelper,
        array                           $data = []
    )
    {
        $this->weatherArchiveCollectionFactory = $weatherArchiveCollectionFactory;
        $this->configHelper = $configHelper;
        parent::__construct($context, $data);
    }

    public function isModuleEnabled(): bool
    {
        return $this->configHelper->getGeneralConfig()->isEnable();
    }

    /**
     * @return WeatherArchiveModel[]
     */
    public function getWeatherArchivesByDate(\DateTimeInterface $dateTime): array
    {
        /** @var WeatherArchiveCollection $weatherArchiveCollection */
        $weatherArchiveCollection = $this->weatherArchiveCollectionFactory->create();
        $weatherArchiveCollection
            ->addOrder(
                'created_at',
                CriteriaInterface::SORT_ORDER_DESC
            )
            ->addFieldToFilter(
                WeatherArchiveInterface::CREATED_AT,
                ['gteq' => $dateTime->format('Y-m-d 00:00:00')])
            ->addFieldToSelect('*')
            ->load();

        return $weatherArchiveCollection->getItems();
    }

    public function getLastWeatherArchive(): ?WeatherArchiveModel
    {
        /** @var WeatherArchiveCollection $weatherArchiveCollection */
        $weatherArchiveCollection = $this->weatherArchiveCollectionFactory->create();

        /** @var null|WeatherArchiveModel $lastWeatherArchive */
        $lastWeatherArchive = $weatherArchiveCollection->getSize() ? $weatherArchiveCollection->getLastItem() : null;

        return $lastWeatherArchive;
    }

    public function getWeatherObject(WeatherArchiveModel $weatherArchive): CurrentWeather
    {
        return (new WeatherFactory())::create($weatherArchive, $this->configHelper->getGeneralConfig());
    }
}
